# Arch Linux template for Packer

Automates creating [Arch Linux](https://www.archlinux.org/) master image using [Packer](https://www.packer.io/) with QEMU/KVM builder.

## Usage

Download Arch Linux ISO installation image from https://www.archlinux.org/download/ and then run command:

```fish
./packer.sh build -var archiso=archlinux.iso archmaster.packer.json
```

The image creation takes around 2.5min. The disk image will be placed to `output/packer-archmaster` (format is [qcow2](https://en.wikipedia.org/wiki/Qcow)).

### Using a Pacman cache

The installation has been configured to try to use Packer's built-in HTTP server as a package mirror (it will work without). The produced image will not have this mirror configured.  
An example follows where the host system's Pacman package cache is used. The packages needed during installation are downloaded there.

```fish
# Force downloading all packages and their dependencies
mkdir /tmp/pacmandb
sudo pacman -Syw --noconfirm --dbpath /tmp/pacmandb/ base syslinux gptfdisk openssh

# Make the package cache available via Packer's HTTP server
mkdir pacman_cache
mount -t overlay overlay -o ro,lowerdir=/var/cache/pacman/pkg/:/tmp/pacmandb/sync/ pacman_cache/

```

## Purpose and design decisions

The purpose of this template is to create a master disk image to be cloned into a more specialized VM image. Created master image can be customized further by using the resulting disk image as the installation media (Packer supports this).

The master image has the following parameters:

* **Disk:** 20 GiB disk (disk image will be compacted automatically).
* **Partitions:**: GPT, 2 partitions:  
  1\. label "boot": ext4, -O ^64bit (for syslinux compatibility), attribute:"legacy BIOS bootable"  
  2\. label "root": ext4
* **Packages:** base syslinux gptfdisk openssh
* **Networking:** Kernel interface naming and DHCP for eth0 using systemd-networkd. IPv4 & IPv6.
* **OpenSSH server:** Allows root login with password ("root").
* **Timezone:** UTC
* **Locale:** Mixed en_US.utf8 and en_DK.utf8 to make dates and times sortable YYYY-MM-DD and 24h, while still using english.
* **SystemD journal:** Max disk usage: 32M, max memory usage: 64MB.
* **mkinitcpio:** Removed generating fallback initramfs image, "autodetect" removed from HOOKS, virtios added to MODULES.
* **Hostname:** archmaster
* **Packages explicitly removed:**  
  dhcpcd jfsutils man-db man-pages netctl openresolv pciutils pcmciautils reiserfsprogs s-nail texinfo usbutils vi xfsprogs
* **Systemd machine-id:** automatically generated on first boot.
* **OpenSSH server keys:** automatically generated on first boot.

### Checklist for cloned VM image

- [ ] hostname
- [ ] mac address
- [ ] ssh user auth

This list might be non-exhaustive. See [virt-sysprep](http://libguestfs.org/virt-sysprep.1.html) for cleaning the image before cloning.

## Example QEMU command to run the image

```
$ qemu-system-x86_64 -machine type=pc,accel=kvm -m 2048M -name packer-qemu -netdev user,id=user.0,hostfwd=tcp::3213-:22 -display sdl -vga std -device virtio-net,netdev=user.0 -drive file=output/packer-archmaster,if=virtio,cache=writeback
```

## See also

https://github.com/elasticdog/packer-arch

## License

[UNLICENSE](UNLICENSE)  
Mention of origin would be appreciated.