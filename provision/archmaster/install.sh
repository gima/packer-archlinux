#!/bin/bash
set -o xtrace

cd /tmp

echo -- get dependencies
curl -O "http://${PACKER_HTTP_ADDR}/provision/archmaster/install2.sh"

echo -- create partitions, set first bootable
sgdisk --new 1:0:+128M --attributes 1:set:2 --new 2:0:0 /dev/sda

echo -- format partition
mkfs.ext4 -L boot -O '^64bit' /dev/sda1
mkfs.ext4 -L root /dev/sda2
partprobe

echo -- mount fs
mount -o noatime,errors=remount-ro /dev/disk/by-label/root /mnt/
mkdir /mnt/boot/
mount -o noatime,errors=remount-ro /dev/disk/by-label/boot /mnt/boot/

echo -- configure pacman mirrors
cat << EOF > /etc/pacman.d/mirrorlist
Server = http://${PACKER_HTTP_ADDR}/pacman_cache
Server = https://mirrors.kernel.org/archlinux/\$repo/os/\$arch
EOF

echo -- install packages
pacstrap /mnt base gptfdisk syslinux openssh

echo -- run install script in chroot
cat install2.sh | arch-chroot /mnt bash -
genfstab -L /mnt > /mnt/etc/fstab

echo -- cleanup
sync
fstrim -a -v
umount -R /mnt
